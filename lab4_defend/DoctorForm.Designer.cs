﻿namespace lab4_defend
{
    partial class DoctorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_name = new System.Windows.Forms.Label();
            this.textBox_name = new System.Windows.Forms.TextBox();
            this.textBox_surname = new System.Windows.Forms.TextBox();
            this.label_surname = new System.Windows.Forms.Label();
            this.textBox_patronymic = new System.Windows.Forms.TextBox();
            this.label_patronymic = new System.Windows.Forms.Label();
            this.label_age = new System.Windows.Forms.Label();
            this.numericUpDown_age = new System.Windows.Forms.NumericUpDown();
            this.label_hospital = new System.Windows.Forms.Label();
            this.listBox_hospital = new System.Windows.Forms.ListBox();
            this.textBox_position = new System.Windows.Forms.TextBox();
            this.label_position = new System.Windows.Forms.Label();
            this.button_ok = new System.Windows.Forms.Button();
            this.button_cancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_age)).BeginInit();
            this.SuspendLayout();
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Location = new System.Drawing.Point(13, 16);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(32, 13);
            this.label_name.TabIndex = 0;
            this.label_name.Text = "Имя:";
            // 
            // textBox_name
            // 
            this.textBox_name.Location = new System.Drawing.Point(128, 13);
            this.textBox_name.Name = "textBox_name";
            this.textBox_name.Size = new System.Drawing.Size(185, 20);
            this.textBox_name.TabIndex = 1;
            // 
            // textBox_surname
            // 
            this.textBox_surname.Location = new System.Drawing.Point(128, 39);
            this.textBox_surname.Name = "textBox_surname";
            this.textBox_surname.Size = new System.Drawing.Size(185, 20);
            this.textBox_surname.TabIndex = 3;
            // 
            // label_surname
            // 
            this.label_surname.AutoSize = true;
            this.label_surname.Location = new System.Drawing.Point(13, 42);
            this.label_surname.Name = "label_surname";
            this.label_surname.Size = new System.Drawing.Size(59, 13);
            this.label_surname.TabIndex = 2;
            this.label_surname.Text = "Фамилия:";
            // 
            // textBox_patronymic
            // 
            this.textBox_patronymic.Location = new System.Drawing.Point(128, 65);
            this.textBox_patronymic.Name = "textBox_patronymic";
            this.textBox_patronymic.Size = new System.Drawing.Size(185, 20);
            this.textBox_patronymic.TabIndex = 5;
            // 
            // label_patronymic
            // 
            this.label_patronymic.AutoSize = true;
            this.label_patronymic.Location = new System.Drawing.Point(13, 68);
            this.label_patronymic.Name = "label_patronymic";
            this.label_patronymic.Size = new System.Drawing.Size(57, 13);
            this.label_patronymic.TabIndex = 4;
            this.label_patronymic.Text = "Отчество:";
            // 
            // label_age
            // 
            this.label_age.AutoSize = true;
            this.label_age.Location = new System.Drawing.Point(13, 94);
            this.label_age.Name = "label_age";
            this.label_age.Size = new System.Drawing.Size(52, 13);
            this.label_age.TabIndex = 6;
            this.label_age.Text = "Возраст:";
            // 
            // numericUpDown_age
            // 
            this.numericUpDown_age.Location = new System.Drawing.Point(128, 92);
            this.numericUpDown_age.Name = "numericUpDown_age";
            this.numericUpDown_age.Size = new System.Drawing.Size(96, 20);
            this.numericUpDown_age.TabIndex = 7;
            // 
            // label_hospital
            // 
            this.label_hospital.AutoSize = true;
            this.label_hospital.Location = new System.Drawing.Point(13, 144);
            this.label_hospital.Name = "label_hospital";
            this.label_hospital.Size = new System.Drawing.Size(59, 13);
            this.label_hospital.TabIndex = 8;
            this.label_hospital.Text = "Больница:";
            // 
            // listBox_hospital
            // 
            this.listBox_hospital.FormattingEnabled = true;
            this.listBox_hospital.Location = new System.Drawing.Point(128, 144);
            this.listBox_hospital.Name = "listBox_hospital";
            this.listBox_hospital.Size = new System.Drawing.Size(185, 82);
            this.listBox_hospital.TabIndex = 9;
            // 
            // textBox_position
            // 
            this.textBox_position.Location = new System.Drawing.Point(128, 118);
            this.textBox_position.Name = "textBox_position";
            this.textBox_position.Size = new System.Drawing.Size(185, 20);
            this.textBox_position.TabIndex = 11;
            // 
            // label_position
            // 
            this.label_position.AutoSize = true;
            this.label_position.Location = new System.Drawing.Point(13, 121);
            this.label_position.Name = "label_position";
            this.label_position.Size = new System.Drawing.Size(88, 13);
            this.label_position.TabIndex = 10;
            this.label_position.Text = "Специальность:";
            // 
            // button_ok
            // 
            this.button_ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button_ok.Location = new System.Drawing.Point(157, 232);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(75, 23);
            this.button_ok.TabIndex = 12;
            this.button_ok.Text = "Ок";
            this.button_ok.UseVisualStyleBackColor = true;
            // 
            // button_cancel
            // 
            this.button_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button_cancel.Location = new System.Drawing.Point(238, 232);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(75, 23);
            this.button_cancel.TabIndex = 13;
            this.button_cancel.Text = "Отмена";
            this.button_cancel.UseVisualStyleBackColor = true;
            // 
            // PersonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(330, 263);
            this.Controls.Add(this.button_cancel);
            this.Controls.Add(this.button_ok);
            this.Controls.Add(this.textBox_position);
            this.Controls.Add(this.label_position);
            this.Controls.Add(this.listBox_hospital);
            this.Controls.Add(this.label_hospital);
            this.Controls.Add(this.numericUpDown_age);
            this.Controls.Add(this.label_age);
            this.Controls.Add(this.textBox_patronymic);
            this.Controls.Add(this.label_patronymic);
            this.Controls.Add(this.textBox_surname);
            this.Controls.Add(this.label_surname);
            this.Controls.Add(this.textBox_name);
            this.Controls.Add(this.label_name);
            this.MinimumSize = new System.Drawing.Size(346, 275);
            this.Name = "PersonForm";
            this.Text = "Доктор";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_age)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.Label label_surname;
        private System.Windows.Forms.Label label_patronymic;
        private System.Windows.Forms.Label label_age;
        private System.Windows.Forms.Label label_hospital;
        protected internal System.Windows.Forms.ListBox listBox_hospital;
        private System.Windows.Forms.Label label_position;
        protected internal System.Windows.Forms.TextBox textBox_name;
        protected internal System.Windows.Forms.TextBox textBox_surname;
        protected internal System.Windows.Forms.TextBox textBox_patronymic;
        protected internal System.Windows.Forms.NumericUpDown numericUpDown_age;
        protected internal System.Windows.Forms.TextBox textBox_position;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.Button button_cancel;
    }
}