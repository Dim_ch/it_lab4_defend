﻿namespace lab4_defend
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle85 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            this.test_page = new System.Windows.Forms.TabPage();
            this.delete_test_button = new System.Windows.Forms.Button();
            this.change_test_button = new System.Windows.Forms.Button();
            this.add_test_button = new System.Windows.Forms.Button();
            this.test_GridView = new System.Windows.Forms.DataGridView();
            this.equip_page = new System.Windows.Forms.TabPage();
            this.delete_equip_button = new System.Windows.Forms.Button();
            this.change_equip_button = new System.Windows.Forms.Button();
            this.add_equip_button = new System.Windows.Forms.Button();
            this.equip_GridView = new System.Windows.Forms.DataGridView();
            this.patient_page = new System.Windows.Forms.TabPage();
            this.delete_patient_button = new System.Windows.Forms.Button();
            this.change_patient_button = new System.Windows.Forms.Button();
            this.add_patient_button = new System.Windows.Forms.Button();
            this.patient_GridView = new System.Windows.Forms.DataGridView();
            this.doctor_page = new System.Windows.Forms.TabPage();
            this.delete_doc_button = new System.Windows.Forms.Button();
            this.change_doc_button = new System.Windows.Forms.Button();
            this.add_doc_button = new System.Windows.Forms.Button();
            this.doctors_GridView = new System.Windows.Forms.DataGridView();
            this.hospital_page = new System.Windows.Forms.TabPage();
            this.delete_hospital_button = new System.Windows.Forms.Button();
            this.change_hospital_button = new System.Windows.Forms.Button();
            this.add_hospital_button = new System.Windows.Forms.Button();
            this.hospital_dataGridView = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.test_page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.test_GridView)).BeginInit();
            this.equip_page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.equip_GridView)).BeginInit();
            this.patient_page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.patient_GridView)).BeginInit();
            this.doctor_page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.doctors_GridView)).BeginInit();
            this.hospital_page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hospital_dataGridView)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // test_page
            // 
            this.test_page.Controls.Add(this.delete_test_button);
            this.test_page.Controls.Add(this.change_test_button);
            this.test_page.Controls.Add(this.add_test_button);
            this.test_page.Controls.Add(this.test_GridView);
            this.test_page.Location = new System.Drawing.Point(4, 22);
            this.test_page.Name = "test_page";
            this.test_page.Padding = new System.Windows.Forms.Padding(5);
            this.test_page.Size = new System.Drawing.Size(437, 204);
            this.test_page.TabIndex = 4;
            this.test_page.Text = "Пробы";
            this.test_page.UseVisualStyleBackColor = true;
            // 
            // delete_test_button
            // 
            this.delete_test_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.delete_test_button.Location = new System.Drawing.Point(170, 164);
            this.delete_test_button.Name = "delete_test_button";
            this.delete_test_button.Size = new System.Drawing.Size(75, 31);
            this.delete_test_button.TabIndex = 15;
            this.delete_test_button.Text = "Удалить";
            this.delete_test_button.UseVisualStyleBackColor = true;
            this.delete_test_button.Click += new System.EventHandler(this.delete_test_button_Click);
            // 
            // change_test_button
            // 
            this.change_test_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.change_test_button.Location = new System.Drawing.Point(89, 164);
            this.change_test_button.Name = "change_test_button";
            this.change_test_button.Size = new System.Drawing.Size(75, 31);
            this.change_test_button.TabIndex = 14;
            this.change_test_button.Text = "Изменить";
            this.change_test_button.UseVisualStyleBackColor = true;
            this.change_test_button.Click += new System.EventHandler(this.change_test_button_Click);
            // 
            // add_test_button
            // 
            this.add_test_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.add_test_button.Location = new System.Drawing.Point(8, 164);
            this.add_test_button.Name = "add_test_button";
            this.add_test_button.Size = new System.Drawing.Size(75, 31);
            this.add_test_button.TabIndex = 13;
            this.add_test_button.Text = "Добавить";
            this.add_test_button.UseVisualStyleBackColor = true;
            this.add_test_button.Click += new System.EventHandler(this.add_test_button_Click);
            // 
            // test_GridView
            // 
            this.test_GridView.AllowUserToAddRows = false;
            this.test_GridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle81.BackColor = System.Drawing.Color.Silver;
            this.test_GridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle81;
            this.test_GridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.test_GridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.test_GridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.test_GridView.Location = new System.Drawing.Point(8, 8);
            this.test_GridView.Name = "test_GridView";
            this.test_GridView.ReadOnly = true;
            dataGridViewCellStyle82.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle82.Padding = new System.Windows.Forms.Padding(2);
            this.test_GridView.RowsDefaultCellStyle = dataGridViewCellStyle82;
            this.test_GridView.Size = new System.Drawing.Size(420, 150);
            this.test_GridView.TabIndex = 12;
            this.test_GridView.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.test_GridView_DataBindingComplete);
            // 
            // equip_page
            // 
            this.equip_page.Controls.Add(this.delete_equip_button);
            this.equip_page.Controls.Add(this.change_equip_button);
            this.equip_page.Controls.Add(this.add_equip_button);
            this.equip_page.Controls.Add(this.equip_GridView);
            this.equip_page.Location = new System.Drawing.Point(4, 22);
            this.equip_page.Name = "equip_page";
            this.equip_page.Padding = new System.Windows.Forms.Padding(5);
            this.equip_page.Size = new System.Drawing.Size(437, 204);
            this.equip_page.TabIndex = 3;
            this.equip_page.Text = "Оборудование";
            this.equip_page.UseVisualStyleBackColor = true;
            // 
            // delete_equip_button
            // 
            this.delete_equip_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.delete_equip_button.Location = new System.Drawing.Point(170, 164);
            this.delete_equip_button.Name = "delete_equip_button";
            this.delete_equip_button.Size = new System.Drawing.Size(75, 31);
            this.delete_equip_button.TabIndex = 11;
            this.delete_equip_button.Text = "Удалить";
            this.delete_equip_button.UseVisualStyleBackColor = true;
            this.delete_equip_button.Click += new System.EventHandler(this.delete_equip_button_Click);
            // 
            // change_equip_button
            // 
            this.change_equip_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.change_equip_button.Location = new System.Drawing.Point(89, 164);
            this.change_equip_button.Name = "change_equip_button";
            this.change_equip_button.Size = new System.Drawing.Size(75, 31);
            this.change_equip_button.TabIndex = 10;
            this.change_equip_button.Text = "Изменить";
            this.change_equip_button.UseVisualStyleBackColor = true;
            this.change_equip_button.Click += new System.EventHandler(this.change_equip_button_Click);
            // 
            // add_equip_button
            // 
            this.add_equip_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.add_equip_button.Location = new System.Drawing.Point(8, 164);
            this.add_equip_button.Name = "add_equip_button";
            this.add_equip_button.Size = new System.Drawing.Size(75, 31);
            this.add_equip_button.TabIndex = 9;
            this.add_equip_button.Text = "Добавить";
            this.add_equip_button.UseVisualStyleBackColor = true;
            this.add_equip_button.Click += new System.EventHandler(this.add_equip_button_Click);
            // 
            // equip_GridView
            // 
            this.equip_GridView.AllowUserToAddRows = false;
            this.equip_GridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle83.BackColor = System.Drawing.Color.Silver;
            this.equip_GridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle83;
            this.equip_GridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.equip_GridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.equip_GridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.equip_GridView.Location = new System.Drawing.Point(8, 8);
            this.equip_GridView.Name = "equip_GridView";
            this.equip_GridView.ReadOnly = true;
            dataGridViewCellStyle84.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle84.Padding = new System.Windows.Forms.Padding(2);
            this.equip_GridView.RowsDefaultCellStyle = dataGridViewCellStyle84;
            this.equip_GridView.Size = new System.Drawing.Size(420, 150);
            this.equip_GridView.TabIndex = 8;
            // 
            // patient_page
            // 
            this.patient_page.Controls.Add(this.delete_patient_button);
            this.patient_page.Controls.Add(this.change_patient_button);
            this.patient_page.Controls.Add(this.add_patient_button);
            this.patient_page.Controls.Add(this.patient_GridView);
            this.patient_page.Location = new System.Drawing.Point(4, 22);
            this.patient_page.Name = "patient_page";
            this.patient_page.Padding = new System.Windows.Forms.Padding(5);
            this.patient_page.Size = new System.Drawing.Size(437, 204);
            this.patient_page.TabIndex = 2;
            this.patient_page.Text = "Пациенты";
            this.patient_page.UseVisualStyleBackColor = true;
            // 
            // delete_patient_button
            // 
            this.delete_patient_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.delete_patient_button.Location = new System.Drawing.Point(170, 164);
            this.delete_patient_button.Name = "delete_patient_button";
            this.delete_patient_button.Size = new System.Drawing.Size(75, 31);
            this.delete_patient_button.TabIndex = 7;
            this.delete_patient_button.Text = "Удалить";
            this.delete_patient_button.UseVisualStyleBackColor = true;
            this.delete_patient_button.Click += new System.EventHandler(this.delete_patient_button_Click);
            // 
            // change_patient_button
            // 
            this.change_patient_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.change_patient_button.Location = new System.Drawing.Point(89, 164);
            this.change_patient_button.Name = "change_patient_button";
            this.change_patient_button.Size = new System.Drawing.Size(75, 31);
            this.change_patient_button.TabIndex = 6;
            this.change_patient_button.Text = "Изменить";
            this.change_patient_button.UseVisualStyleBackColor = true;
            this.change_patient_button.Click += new System.EventHandler(this.change_patient_button_Click);
            // 
            // add_patient_button
            // 
            this.add_patient_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.add_patient_button.Location = new System.Drawing.Point(8, 164);
            this.add_patient_button.Name = "add_patient_button";
            this.add_patient_button.Size = new System.Drawing.Size(75, 31);
            this.add_patient_button.TabIndex = 5;
            this.add_patient_button.Text = "Добавить";
            this.add_patient_button.UseVisualStyleBackColor = true;
            this.add_patient_button.Click += new System.EventHandler(this.add_patient_button_Click);
            // 
            // patient_GridView
            // 
            this.patient_GridView.AllowUserToAddRows = false;
            this.patient_GridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle85.BackColor = System.Drawing.Color.Silver;
            this.patient_GridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle85;
            this.patient_GridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.patient_GridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.patient_GridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.patient_GridView.Location = new System.Drawing.Point(8, 8);
            this.patient_GridView.Name = "patient_GridView";
            this.patient_GridView.ReadOnly = true;
            dataGridViewCellStyle86.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle86.Padding = new System.Windows.Forms.Padding(2);
            this.patient_GridView.RowsDefaultCellStyle = dataGridViewCellStyle86;
            this.patient_GridView.Size = new System.Drawing.Size(420, 150);
            this.patient_GridView.TabIndex = 4;
            this.patient_GridView.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.patient_GridView_DataBindingComplete);
            // 
            // doctor_page
            // 
            this.doctor_page.Controls.Add(this.delete_doc_button);
            this.doctor_page.Controls.Add(this.change_doc_button);
            this.doctor_page.Controls.Add(this.add_doc_button);
            this.doctor_page.Controls.Add(this.doctors_GridView);
            this.doctor_page.Location = new System.Drawing.Point(4, 22);
            this.doctor_page.Name = "doctor_page";
            this.doctor_page.Padding = new System.Windows.Forms.Padding(5);
            this.doctor_page.Size = new System.Drawing.Size(437, 204);
            this.doctor_page.TabIndex = 0;
            this.doctor_page.Text = "Доктора";
            this.doctor_page.UseVisualStyleBackColor = true;
            // 
            // delete_doc_button
            // 
            this.delete_doc_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.delete_doc_button.Location = new System.Drawing.Point(170, 164);
            this.delete_doc_button.MaximumSize = new System.Drawing.Size(250, 31);
            this.delete_doc_button.MinimumSize = new System.Drawing.Size(75, 31);
            this.delete_doc_button.Name = "delete_doc_button";
            this.delete_doc_button.Size = new System.Drawing.Size(75, 31);
            this.delete_doc_button.TabIndex = 3;
            this.delete_doc_button.Text = "Удалить";
            this.delete_doc_button.UseVisualStyleBackColor = true;
            this.delete_doc_button.Click += new System.EventHandler(this.delete_doc_button_Click);
            // 
            // change_doc_button
            // 
            this.change_doc_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.change_doc_button.Location = new System.Drawing.Point(89, 164);
            this.change_doc_button.MaximumSize = new System.Drawing.Size(250, 31);
            this.change_doc_button.MinimumSize = new System.Drawing.Size(75, 31);
            this.change_doc_button.Name = "change_doc_button";
            this.change_doc_button.Size = new System.Drawing.Size(75, 31);
            this.change_doc_button.TabIndex = 2;
            this.change_doc_button.Text = "Изменить";
            this.change_doc_button.UseVisualStyleBackColor = true;
            this.change_doc_button.Click += new System.EventHandler(this.change_doc_button_Click);
            // 
            // add_doc_button
            // 
            this.add_doc_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.add_doc_button.Location = new System.Drawing.Point(8, 164);
            this.add_doc_button.MaximumSize = new System.Drawing.Size(250, 31);
            this.add_doc_button.MinimumSize = new System.Drawing.Size(75, 31);
            this.add_doc_button.Name = "add_doc_button";
            this.add_doc_button.Size = new System.Drawing.Size(75, 31);
            this.add_doc_button.TabIndex = 1;
            this.add_doc_button.Text = "Добавить";
            this.add_doc_button.UseVisualStyleBackColor = true;
            this.add_doc_button.Click += new System.EventHandler(this.add_doc_button_Click);
            // 
            // doctors_GridView
            // 
            this.doctors_GridView.AllowUserToAddRows = false;
            this.doctors_GridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle87.BackColor = System.Drawing.Color.Silver;
            this.doctors_GridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle87;
            this.doctors_GridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.doctors_GridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.doctors_GridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.doctors_GridView.Location = new System.Drawing.Point(8, 8);
            this.doctors_GridView.Name = "doctors_GridView";
            this.doctors_GridView.ReadOnly = true;
            dataGridViewCellStyle88.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle88.Padding = new System.Windows.Forms.Padding(2);
            this.doctors_GridView.RowsDefaultCellStyle = dataGridViewCellStyle88;
            this.doctors_GridView.Size = new System.Drawing.Size(420, 150);
            this.doctors_GridView.TabIndex = 0;
            this.doctors_GridView.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.doctors_GridView_DataBindingComplete);
            // 
            // hospital_page
            // 
            this.hospital_page.Controls.Add(this.delete_hospital_button);
            this.hospital_page.Controls.Add(this.change_hospital_button);
            this.hospital_page.Controls.Add(this.add_hospital_button);
            this.hospital_page.Controls.Add(this.hospital_dataGridView);
            this.hospital_page.Location = new System.Drawing.Point(4, 22);
            this.hospital_page.Name = "hospital_page";
            this.hospital_page.Padding = new System.Windows.Forms.Padding(5);
            this.hospital_page.Size = new System.Drawing.Size(437, 204);
            this.hospital_page.TabIndex = 1;
            this.hospital_page.Text = "Больницы";
            this.hospital_page.UseVisualStyleBackColor = true;
            // 
            // delete_hospital_button
            // 
            this.delete_hospital_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.delete_hospital_button.Location = new System.Drawing.Point(170, 164);
            this.delete_hospital_button.MaximumSize = new System.Drawing.Size(250, 31);
            this.delete_hospital_button.MinimumSize = new System.Drawing.Size(75, 31);
            this.delete_hospital_button.Name = "delete_hospital_button";
            this.delete_hospital_button.Size = new System.Drawing.Size(75, 31);
            this.delete_hospital_button.TabIndex = 8;
            this.delete_hospital_button.Text = "Удалить";
            this.delete_hospital_button.UseVisualStyleBackColor = true;
            this.delete_hospital_button.Click += new System.EventHandler(this.delete_hospital_button_Click);
            // 
            // change_hospital_button
            // 
            this.change_hospital_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.change_hospital_button.Location = new System.Drawing.Point(89, 164);
            this.change_hospital_button.MaximumSize = new System.Drawing.Size(250, 31);
            this.change_hospital_button.MinimumSize = new System.Drawing.Size(75, 31);
            this.change_hospital_button.Name = "change_hospital_button";
            this.change_hospital_button.Size = new System.Drawing.Size(75, 31);
            this.change_hospital_button.TabIndex = 7;
            this.change_hospital_button.Text = "Изменить";
            this.change_hospital_button.UseVisualStyleBackColor = true;
            this.change_hospital_button.Click += new System.EventHandler(this.change_hospital_button_Click);
            // 
            // add_hospital_button
            // 
            this.add_hospital_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.add_hospital_button.Location = new System.Drawing.Point(8, 164);
            this.add_hospital_button.MaximumSize = new System.Drawing.Size(250, 31);
            this.add_hospital_button.MinimumSize = new System.Drawing.Size(75, 31);
            this.add_hospital_button.Name = "add_hospital_button";
            this.add_hospital_button.Size = new System.Drawing.Size(75, 31);
            this.add_hospital_button.TabIndex = 6;
            this.add_hospital_button.Text = "Добавить";
            this.add_hospital_button.UseVisualStyleBackColor = true;
            this.add_hospital_button.Click += new System.EventHandler(this.add_hospital_button_Click);
            // 
            // hospital_dataGridView
            // 
            this.hospital_dataGridView.AllowUserToAddRows = false;
            this.hospital_dataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle89.BackColor = System.Drawing.Color.Silver;
            this.hospital_dataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle89;
            this.hospital_dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hospital_dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.hospital_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.hospital_dataGridView.Location = new System.Drawing.Point(8, 8);
            this.hospital_dataGridView.Name = "hospital_dataGridView";
            this.hospital_dataGridView.ReadOnly = true;
            dataGridViewCellStyle90.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle90.Padding = new System.Windows.Forms.Padding(2);
            this.hospital_dataGridView.RowsDefaultCellStyle = dataGridViewCellStyle90;
            this.hospital_dataGridView.Size = new System.Drawing.Size(420, 150);
            this.hospital_dataGridView.TabIndex = 5;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.hospital_page);
            this.tabControl1.Controls.Add(this.doctor_page);
            this.tabControl1.Controls.Add(this.patient_page);
            this.tabControl1.Controls.Add(this.equip_page);
            this.tabControl1.Controls.Add(this.test_page);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(445, 230);
            this.tabControl1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(445, 230);
            this.Controls.Add(this.tabControl1);
            this.MinimumSize = new System.Drawing.Size(461, 269);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.test_page.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.test_GridView)).EndInit();
            this.equip_page.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.equip_GridView)).EndInit();
            this.patient_page.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.patient_GridView)).EndInit();
            this.doctor_page.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.doctors_GridView)).EndInit();
            this.hospital_page.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.hospital_dataGridView)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage test_page;
        private System.Windows.Forms.Button delete_test_button;
        private System.Windows.Forms.Button change_test_button;
        private System.Windows.Forms.Button add_test_button;
        private System.Windows.Forms.DataGridView test_GridView;
        private System.Windows.Forms.TabPage equip_page;
        private System.Windows.Forms.Button delete_equip_button;
        private System.Windows.Forms.Button change_equip_button;
        private System.Windows.Forms.Button add_equip_button;
        private System.Windows.Forms.DataGridView equip_GridView;
        private System.Windows.Forms.TabPage patient_page;
        private System.Windows.Forms.Button delete_patient_button;
        private System.Windows.Forms.Button change_patient_button;
        private System.Windows.Forms.Button add_patient_button;
        private System.Windows.Forms.DataGridView patient_GridView;
        private System.Windows.Forms.TabPage doctor_page;
        private System.Windows.Forms.Button delete_doc_button;
        private System.Windows.Forms.Button change_doc_button;
        private System.Windows.Forms.Button add_doc_button;
        private System.Windows.Forms.DataGridView doctors_GridView;
        internal System.Windows.Forms.TabPage hospital_page;
        private System.Windows.Forms.DataGridView hospital_dataGridView;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button delete_hospital_button;
        private System.Windows.Forms.Button change_hospital_button;
        private System.Windows.Forms.Button add_hospital_button;
    }
}

