﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Microsoft.EntityFrameworkCore;
using lab3_defend;

namespace lab4_defend
{
    public partial class Form1 : Form
    {
        DbAppContext db;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            db = new DbAppContext();

            db.Doctors.Load();
            db.Patients.Load();
            db.Equipments.Load();
            db.Tests.Load();
            db.Hospitals.Load();

            #region Привязка данных к DataGridView.
            doctors_GridView.DataSource = db.Doctors.Local.ToBindingList();
            patient_GridView.DataSource = db.Patients.Local.ToBindingList();
            equip_GridView.DataSource = db.Equipments.Local.ToBindingList();
            test_GridView.DataSource = db.Tests.Local.ToBindingList();
            hospital_dataGridView.DataSource = db.Hospitals.Local.ToBindingList();
            #endregion
        }

        #region Изменение DataGridView.
        private void doctors_GridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            doctors_GridView.Columns["Hospital"].Visible = false;
            doctors_GridView.Columns["Position"].DisplayIndex = 6;
            doctors_GridView.Columns["HospitalId"].DisplayIndex = 7;
        }

        private void patient_GridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            patient_GridView.Columns["Hospital"].Visible = false;
            patient_GridView.Columns["Tests"].Visible = false;
            patient_GridView.Columns["Infected"].DisplayIndex = 7;
            patient_GridView.Columns["HospitalId"].DisplayIndex = 8;
        }

        private void test_GridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            test_GridView.Columns["Patient"].Visible = false;
        }
        #endregion
        //Добавление больницы.
        private void add_hospital_button_Click(object sender, EventArgs e)
        {
            HospitalForm form = new HospitalForm();
            List<Equipment> equipment = db.Equipments.ToList();

            form.listBox_equipment.DataSource = equipment;
            form.listBox_equipment.ValueMember = "Id";
            form.listBox_equipment.DisplayMember = "Name";
            form.listBox_equipment.SelectedIndex = -1;

            if (form.ShowDialog(this) == DialogResult.Cancel) return;

            Hospital hospital = new Hospital();
            hospital.Name = form.textBox_name.Text;

            foreach (Equipment equip in form.listBox_equipment.SelectedItems)
                hospital.HospitalEquipment.Add(new HospitalEquipment { 
                                                EquipmentId = equip.Id,
                                                HospitalId = hospital.Id
                                                });
            db.Hospitals.Add(hospital);
            db.SaveChanges();
        }
        //Удаление больницы.
        private void delete_hospital_button_Click(object sender, EventArgs e)
        {
            if (hospital_dataGridView.SelectedRows.Count < 1) return;
            int index = hospital_dataGridView.SelectedRows[0].Index;

            if (!Int32.TryParse(hospital_dataGridView[0, index].Value.ToString(), out int id)) return;

            Hospital hospital = db.Hospitals.Find(id);
            db.Hospitals.Remove(hospital);
            db.SaveChanges();
        }
        //Изменение данных больницы.
        private void change_hospital_button_Click(object sender, EventArgs e)
        {
            if (hospital_dataGridView.SelectedRows.Count < 1) return;
            int index = hospital_dataGridView.SelectedRows[0].Index, id;

            if (!Int32.TryParse(hospital_dataGridView[0, index].Value.ToString(), out id)) return;

            Hospital hospital = db.Hospitals.Include(h => h.HospitalEquipment).First(h => h.Id == id);
            HospitalForm form = new HospitalForm();
            List<Equipment> equipment = db.Equipments.ToList();
            List<Equipment> selected_equip = new List<Equipment>();
            
            form.textBox_name.Text = hospital.Name;
            form.listBox_equipment.DataSource = equipment;
            form.listBox_equipment.ValueMember = "Id";
            form.listBox_equipment.DisplayMember = "Name";
            form.listBox_equipment.SelectedIndex = -1;
            //Выделение оборудования.
            foreach (HospitalEquipment item in hospital.HospitalEquipment)
            {
                form.listBox_equipment.SelectedItem = item.Equipment;
                selected_equip.Add(item.Equipment);
            }

            if (form.ShowDialog(this) == DialogResult.Cancel) return;
            //Заполнение форм.
            hospital.Name = form.textBox_name.Text;

            foreach (Equipment eq in equipment)
            {
                //Больница выделена.
                if (form.listBox_equipment.SelectedItems.Contains(eq))
                {
                    if (!selected_equip.Contains(eq))
                        hospital.HospitalEquipment.Add(new HospitalEquipment {
                                                                                HospitalId = hospital.Id,
                                                                                EquipmentId = eq.Id
                                                                              });
                }
                else
                {
                    //Выделение снято.
                    if (selected_equip.Contains(eq))
                    {
                        foreach (HospitalEquipment equip in hospital.HospitalEquipment)
                        {
                            if (equip.Equipment == eq)
                            {
                                hospital.HospitalEquipment.Remove(equip);
                                break;
                            }
                        }
                    }
                }
            }

            db.Entry(hospital).State = EntityState.Modified;
            db.SaveChanges();
        }
        //Добавление доктора.
        private void add_doc_button_Click(object sender, EventArgs e)
        {
            DoctorForm form = new DoctorForm();
            List<Hospital> hospitals = db.Hospitals.ToList();

            form.listBox_hospital.DataSource = hospitals;
            form.listBox_hospital.ValueMember = "Id";
            form.listBox_hospital.DisplayMember = "Name";

            if (form.ShowDialog(this) == DialogResult.Cancel) return;
            Doctor doc = new Doctor();
            doc.Name = form.textBox_name.Text;
            doc.Surname = form.textBox_surname.Text;
            doc.Patronymic = form.textBox_patronymic.Text;
            doc.Position = form.textBox_position.Text;
            doc.Age = (int)form.numericUpDown_age.Value;
            if (form.listBox_hospital.SelectedIndex != -1)
                doc.HospitalId = ((Hospital)form.listBox_hospital.SelectedItem).Id;

            db.Doctors.Add(doc);
            db.SaveChanges();
        }
        //Удаление доктора.
        private void delete_doc_button_Click(object sender, EventArgs e)
        {
            if (doctors_GridView.SelectedRows.Count < 1) return;
            int index = doctors_GridView.SelectedRows[0].Index;
            int Id = doctors_GridView.Columns["Id"].Index;

            if (!Int32.TryParse(doctors_GridView[Id, index].Value.ToString(), out int id)) return;

            Doctor doc = db.Doctors.Find(id);
            db.Doctors.Remove(doc);
            db.SaveChanges();
        }
        //Изменение данных о докторе.
        private void change_doc_button_Click(object sender, EventArgs e)
        {
            if (doctors_GridView.SelectedRows.Count < 1) return;
            int index = doctors_GridView.SelectedRows[0].Index;
            int Id = doctors_GridView.Columns["Id"].Index;

            if (!Int32.TryParse(doctors_GridView[Id, index].Value.ToString(), out int id)) return;

            DoctorForm form = new DoctorForm();
            Doctor doc = db.Doctors.Find(id);
            List<Hospital> hospitals = db.Hospitals.ToList();

            form.textBox_name.Text = doc.Name;
            form.textBox_surname.Text = doc.Surname;
            form.textBox_patronymic.Text = doc.Patronymic;
            form.textBox_position.Text = doc.Position;
            form.numericUpDown_age.Value = doc.Age;

            form.listBox_hospital.DataSource = hospitals;
            form.listBox_hospital.ValueMember = "Id";
            form.listBox_hospital.DisplayMember = "Name";
            form.listBox_hospital.SelectedItem = doc.Hospital;

            if (form.ShowDialog(this) == DialogResult.Cancel) return;

            doc.Name = form.textBox_name.Text;
            doc.Surname = form.textBox_surname.Text;
            doc.Patronymic = form.textBox_patronymic.Text;
            doc.Position = form.textBox_position.Text;
            doc.Age = (int)form.numericUpDown_age.Value;


            if (form.listBox_hospital.SelectedIndex == -1)
            {
                doc.HospitalId = null;
            }
            else
            {
                Id = (int)form.listBox_hospital.SelectedValue;
                if (Id != doc.HospitalId)
                    doc.HospitalId = Id;
            }

            db.Entry(doc).State = EntityState.Modified;
            db.SaveChanges();
        }
        //Добавление пациента.
        private void add_patient_button_Click(object sender, EventArgs e)
        {
            PatientForm form = new PatientForm();
            List<Hospital> hospitals = db.Hospitals.ToList();

            form.listBox_hospital.DataSource = hospitals;
            form.listBox_hospital.ValueMember = "Id";
            form.listBox_hospital.DisplayMember = "Name";

            if (form.ShowDialog(this) == DialogResult.Cancel) return;

            Patient patient = new Patient();
            patient.Name = form.textBox_name.Text;
            patient.Surname = form.textBox_surname.Text;
            patient.Patronymic = form.textBox_patronymic.Text;
            patient.Infected = form.checkBox_infected.Checked;
            patient.Age = (int)form.numericUpDown_age.Value;
            if (form.listBox_hospital.SelectedIndex != -1)
                patient.HospitalId = ((Hospital)form.listBox_hospital.SelectedItem).Id;

            db.Patients.Add(patient);
            db.SaveChanges();
        }
        //Удаление пациента.
        private void delete_patient_button_Click(object sender, EventArgs e)
        {
            if (patient_GridView.SelectedRows.Count < 1) return;
            int index = patient_GridView.SelectedRows[0].Index;
            int Id = patient_GridView.Columns["Id"].Index;

            if (!Int32.TryParse(patient_GridView[Id, index].Value.ToString(), out int id)) return;

            Patient patient = db.Patients.Find(id);
            db.Patients.Remove(patient);
            db.SaveChanges();
        }
        //Изменение данных о пациенте.
        private void change_patient_button_Click(object sender, EventArgs e)
        {
            if (patient_GridView.SelectedRows.Count < 1) return;
            int index = patient_GridView.SelectedRows[0].Index;
            int Id = patient_GridView.Columns["Id"].Index;

            if (!Int32.TryParse(patient_GridView[Id, index].Value.ToString(), out int id)) return;

            PatientForm form = new PatientForm();
            Patient patient = db.Patients.Find(id);
            List<Hospital> hospitals = db.Hospitals.ToList();

            form.textBox_name.Text = patient.Name;
            form.textBox_surname.Text = patient.Surname;
            form.textBox_patronymic.Text = patient.Patronymic;
            form.numericUpDown_age.Value = patient.Age;
            if (patient.Infected.HasValue)
                form.checkBox_infected.Checked = (bool)patient.Infected;

            form.listBox_hospital.DataSource = hospitals;
            form.listBox_hospital.ValueMember = "Id";
            form.listBox_hospital.DisplayMember = "Name";
            form.listBox_hospital.SelectedItem = patient.Hospital;

            if (form.ShowDialog(this) == DialogResult.Cancel) return;

            patient.Name = form.textBox_name.Text;
            patient.Surname = form.textBox_surname.Text;
            patient.Patronymic = form.textBox_patronymic.Text;
            patient.Infected = form.checkBox_infected.Checked;
            patient.Age = (int)form.numericUpDown_age.Value;

            if (form.listBox_hospital.SelectedIndex == -1)
            {
                patient.HospitalId = null;
            }
            else
            {
                Id = (int)form.listBox_hospital.SelectedValue;
                if (Id != patient.HospitalId)
                    patient.HospitalId = Id;
            }

            db.Entry(patient).State = EntityState.Modified;
            db.SaveChanges();
        }
        //Добавление оборудования.
        private void add_equip_button_Click(object sender, EventArgs e)
        {
            EquipmentForm form = new EquipmentForm();

            if (form.ShowDialog(this) == DialogResult.Cancel) return;

            Equipment equipment = new Equipment();
            equipment.Name = form.textBox_name.Text;
            equipment.Type = form.textBox_type.Text;

            db.Equipments.Add(equipment);
            db.SaveChanges();

        }
        //Изменение данных об оборудовании.
        private void change_equip_button_Click(object sender, EventArgs e)
        {
            if (equip_GridView.SelectedRows.Count < 1) return;
            int index = equip_GridView.SelectedRows[0].Index;
            int Id = equip_GridView.Columns["Id"].Index;

            if (!Int32.TryParse(equip_GridView[Id, index].Value.ToString(), out int id)) return;

            EquipmentForm form = new EquipmentForm();
            Equipment equip = db.Equipments.Find(id);

            form.textBox_name.Text = equip.Name;
            form.textBox_type.Text = equip.Type;

            if (form.ShowDialog(this) == DialogResult.Cancel) return;

            equip.Name = form.textBox_name.Text;
            equip.Type = form.textBox_type.Text;

            db.Entry(equip).State = EntityState.Modified;
            db.SaveChanges();
        }
        //Удаление оборудования.
        private void delete_equip_button_Click(object sender, EventArgs e)
        {
            if (equip_GridView.SelectedRows.Count < 1) return;
            int index = equip_GridView.SelectedRows[0].Index;
            int Id = equip_GridView.Columns["Id"].Index;

            if (!Int32.TryParse(equip_GridView[Id, index].Value.ToString(), out int id)) return;

            Equipment equip = db.Equipments.Find(id);
            db.Equipments.Remove(equip);
            db.SaveChanges();
        }
        //Добавление пробы.
        private void add_test_button_Click(object sender, EventArgs e)
        {
            TestForm form = new TestForm();
            List<Patient> patients = db.Patients.ToList();

            form.listBox_patient.DataSource = patients;
            form.listBox_patient.ValueMember = "Id";
            form.listBox_patient.DisplayMember = "Id";
            form.listBox_patient.SelectedIndex = 0;
            form.listBox_day.SelectedIndex = 0;

            if (form.ShowDialog(this) == DialogResult.Cancel) return;

            Tests test = new Tests();
            if (form.listBox_day.SelectedIndex != -1)
            {
                int i = form.listBox_day.SelectedIndex;
                test.Day = Int32.Parse(form.listBox_day.Items[i].ToString());
            }
            if (form.listBox_patient.SelectedIndex != -1)
                test.PatientId = (int)form.listBox_patient.SelectedValue;
            test.Patient_test = form.checkBox_infected.Checked;

            db.Tests.Add(test);
            db.SaveChanges();
        }

        private void change_test_button_Click(object sender, EventArgs e)
        {
            if (test_GridView.SelectedRows.Count < 1) return;
            int index = test_GridView.SelectedRows[0].Index;
            int Id = test_GridView.Columns["Id"].Index;

            if (!Int32.TryParse(test_GridView[Id, index].Value.ToString(), out int id)) return;

            TestForm form = new TestForm();
            Tests test = db.Tests.Find(id);
            List<Patient> patients = db.Patients.ToList();

            form.listBox_patient.Enabled = false;
            form.label_patient.Enabled = false;

            form.listBox_patient.DataSource = patients;
            form.listBox_patient.ValueMember = "Id";
            form.listBox_patient.DisplayMember = "Id";
            form.listBox_patient.SelectedValue = test.PatientId;
            form.checkBox_infected.Checked = test.Patient_test;
            form.listBox_day.SelectedItem = test.Day.ToString();

            if (form.ShowDialog(this) == DialogResult.Cancel) return;

            if (form.listBox_patient.SelectedIndex != -1)
                test.PatientId = (int)form.listBox_patient.SelectedValue;
            test.Patient_test = form.checkBox_infected.Checked;
            if (form.listBox_day.SelectedIndex != -1)
            {
                int i = form.listBox_day.SelectedIndex;
                test.Day = Int32.Parse(form.listBox_day.Items[i].ToString());
            }

            db.Entry(test).State = EntityState.Modified;
            db.SaveChanges();
        }

        private void delete_test_button_Click(object sender, EventArgs e)
        {
            if (test_GridView.SelectedRows.Count < 1) return;
            int index = test_GridView.SelectedRows[0].Index;
            int Id = test_GridView.Columns["Id"].Index;

            if (!Int32.TryParse(test_GridView[Id, index].Value.ToString(), out int id)) return;

            Tests test = db.Tests.Find(id);
            db.Tests.Remove(test);
            db.SaveChanges();
        }
    }
}
