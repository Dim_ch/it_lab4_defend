﻿namespace lab4_defend
{
    partial class HospitalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_cancel = new System.Windows.Forms.Button();
            this.button_ok = new System.Windows.Forms.Button();
            this.textBox_name = new System.Windows.Forms.TextBox();
            this.label_name = new System.Windows.Forms.Label();
            this.listBox_equipment = new System.Windows.Forms.ListBox();
            this.label_equipment = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_cancel
            // 
            this.button_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button_cancel.Location = new System.Drawing.Point(242, 142);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(75, 23);
            this.button_cancel.TabIndex = 15;
            this.button_cancel.Text = "Отмена";
            this.button_cancel.UseVisualStyleBackColor = true;
            // 
            // button_ok
            // 
            this.button_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button_ok.Location = new System.Drawing.Point(161, 142);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(75, 23);
            this.button_ok.TabIndex = 14;
            this.button_ok.Text = "Ок";
            this.button_ok.UseVisualStyleBackColor = true;
            // 
            // textBox_name
            // 
            this.textBox_name.Location = new System.Drawing.Point(115, 12);
            this.textBox_name.Name = "textBox_name";
            this.textBox_name.Size = new System.Drawing.Size(185, 20);
            this.textBox_name.TabIndex = 17;
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Location = new System.Drawing.Point(12, 15);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(60, 13);
            this.label_name.TabIndex = 16;
            this.label_name.Text = "Название:";
            // 
            // listBox_equipment
            // 
            this.listBox_equipment.FormattingEnabled = true;
            this.listBox_equipment.Location = new System.Drawing.Point(115, 49);
            this.listBox_equipment.Name = "listBox_equipment";
            this.listBox_equipment.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox_equipment.Size = new System.Drawing.Size(185, 82);
            this.listBox_equipment.TabIndex = 19;
            // 
            // label_equipment
            // 
            this.label_equipment.AutoSize = true;
            this.label_equipment.Location = new System.Drawing.Point(12, 49);
            this.label_equipment.Name = "label_equipment";
            this.label_equipment.Size = new System.Drawing.Size(83, 13);
            this.label_equipment.TabIndex = 18;
            this.label_equipment.Text = "Оборудование:";
            // 
            // HospitalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 177);
            this.Controls.Add(this.listBox_equipment);
            this.Controls.Add(this.label_equipment);
            this.Controls.Add(this.textBox_name);
            this.Controls.Add(this.label_name);
            this.Controls.Add(this.button_cancel);
            this.Controls.Add(this.button_ok);
            this.Name = "HospitalForm";
            this.Text = "HospitalForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.Button button_ok;
        protected internal System.Windows.Forms.TextBox textBox_name;
        private System.Windows.Forms.Label label_name;
        protected internal System.Windows.Forms.ListBox listBox_equipment;
        private System.Windows.Forms.Label label_equipment;
    }
}