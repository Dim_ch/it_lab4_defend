﻿namespace lab4_defend
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_patient = new System.Windows.Forms.Label();
            this.listBox_patient = new System.Windows.Forms.ListBox();
            this.labe_day = new System.Windows.Forms.Label();
            this.listBox_day = new System.Windows.Forms.ListBox();
            this.button_cancel = new System.Windows.Forms.Button();
            this.button_ok = new System.Windows.Forms.Button();
            this.checkBox_infected = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label_patient
            // 
            this.label_patient.Location = new System.Drawing.Point(13, 13);
            this.label_patient.Name = "label_patient";
            this.label_patient.Size = new System.Drawing.Size(87, 82);
            this.label_patient.TabIndex = 0;
            this.label_patient.Text = "Выберите пациента:";
            // 
            // listBox_patient
            // 
            this.listBox_patient.FormattingEnabled = true;
            this.listBox_patient.Location = new System.Drawing.Point(116, 13);
            this.listBox_patient.Name = "listBox_patient";
            this.listBox_patient.Size = new System.Drawing.Size(229, 82);
            this.listBox_patient.TabIndex = 1;
            // 
            // labe_day
            // 
            this.labe_day.AutoSize = true;
            this.labe_day.Location = new System.Drawing.Point(13, 107);
            this.labe_day.Name = "labe_day";
            this.labe_day.Size = new System.Drawing.Size(87, 13);
            this.labe_day.TabIndex = 2;
            this.labe_day.Text = "Выберите день:";
            // 
            // listBox_day
            // 
            this.listBox_day.FormattingEnabled = true;
            this.listBox_day.Items.AddRange(new object[] {
            "1",
            "3",
            "10"});
            this.listBox_day.Location = new System.Drawing.Point(116, 107);
            this.listBox_day.Name = "listBox_day";
            this.listBox_day.Size = new System.Drawing.Size(111, 56);
            this.listBox_day.TabIndex = 3;
            // 
            // button_cancel
            // 
            this.button_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button_cancel.Location = new System.Drawing.Point(274, 191);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(75, 23);
            this.button_cancel.TabIndex = 15;
            this.button_cancel.Text = "Отмена";
            this.button_cancel.UseVisualStyleBackColor = true;
            // 
            // button_ok
            // 
            this.button_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button_ok.Location = new System.Drawing.Point(193, 191);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(75, 23);
            this.button_ok.TabIndex = 14;
            this.button_ok.Text = "Ок";
            this.button_ok.UseVisualStyleBackColor = true;
            // 
            // checkBox_infected
            // 
            this.checkBox_infected.AutoSize = true;
            this.checkBox_infected.Location = new System.Drawing.Point(116, 169);
            this.checkBox_infected.Name = "checkBox_infected";
            this.checkBox_infected.Size = new System.Drawing.Size(71, 17);
            this.checkBox_infected.TabIndex = 16;
            this.checkBox_infected.Text = "Заражён";
            this.checkBox_infected.UseVisualStyleBackColor = true;
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 226);
            this.Controls.Add(this.checkBox_infected);
            this.Controls.Add(this.button_cancel);
            this.Controls.Add(this.button_ok);
            this.Controls.Add(this.listBox_day);
            this.Controls.Add(this.labe_day);
            this.Controls.Add(this.listBox_patient);
            this.Controls.Add(this.label_patient);
            this.Name = "TestForm";
            this.Text = "TestForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labe_day;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.Button button_ok;
        protected internal System.Windows.Forms.ListBox listBox_patient;
        protected internal System.Windows.Forms.ListBox listBox_day;
        protected internal System.Windows.Forms.CheckBox checkBox_infected;
        protected internal System.Windows.Forms.Label label_patient;
    }
}