﻿using Microsoft.EntityFrameworkCore;

namespace lab3_defend
{
    class DbAppContext : DbContext
    {
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Equipment> Equipments { get; set; }
        public DbSet<Tests> Tests { get; set; }
        public DbSet<Hospital> Hospitals { get; set; }

        private string urlDb = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;";
        private string nameDb = "Database=App_covid_19";
        
        public DbAppContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(urlDb + nameDb);
        }
    }
}
