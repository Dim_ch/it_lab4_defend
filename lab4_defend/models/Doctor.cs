﻿namespace lab3_defend
{
    class Doctor : Person
    {
        public string Position { get; set; }

        public int? HospitalId { get; set; }
        public Hospital Hospital { get; set; }
    }
}
