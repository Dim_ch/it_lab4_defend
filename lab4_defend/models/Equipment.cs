﻿using System.Collections.Generic;

namespace lab3_defend
{
    class Equipment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        //public int Amount { get; set; }

        public List<HospitalEquipment> HospitalEquipment { get; set; }
    }
}
