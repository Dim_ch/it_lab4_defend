﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab3_defend
{
    class Hospital
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public List<Patient> Patients { get; set; }
        public List<Doctor> Doctors { get; set; }
        public List<HospitalEquipment> HospitalEquipment{ get; set; }

        public Hospital()
        {
            HospitalEquipment = new List<HospitalEquipment>();
        }
    }
}
