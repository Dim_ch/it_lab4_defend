﻿namespace lab3_defend
{
    class Patient : Person
    {
        public bool? Infected { get; set; }

        public int? HospitalId { get; set; }
        public Hospital Hospital { get; set; }

        public Tests Tests { get; set; }
    }
}
