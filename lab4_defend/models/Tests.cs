﻿namespace lab3_defend
{
    class Tests
    {
        public int Id { get; set; }
        public int Day { get; set; }
        public bool Patient_test { get; set; }

        public int PatientId { get; set; }
        public Patient Patient { get; set; }
    }
}
